import java.util.ArrayList;

public class Minterma {

    private ArrayList<Integer> decimal = new ArrayList<>();
    private ArrayList<Variable> variable = new ArrayList();

    public void setDecimal(ArrayList<Integer> decimal) {
        this.decimal = decimal;
    }

    public void addDecimal(Integer decimalNum){
        decimal.add(decimalNum);
    }

    public ArrayList<Integer> getDecimal() {
        return decimal;
    }

    public void addVariable(Variable variable){
        this.variable.add(variable);
    }

    public ArrayList<Variable> getVariable() {
        return variable;
    }
}
