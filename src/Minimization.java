import java.util.ArrayList;
import java.util.Collections;

public class Minimization {
    private final int[] function;
    private int numVariable;

    //Printer printer = new Printer();

    public Minimization(int[] function, int numVariable) {
        this.function = function;
        this.numVariable = numVariable;
    }

    public int[] convertToBinary(int dec) {
        String str = Integer.toBinaryString(dec); //Integer to BinaryString
        char[] chr = str.toCharArray(); //convert to char Array
        if (chr.length > numVariable) {
            System.out.println("Error, incorrect function value.");
            System.exit(0);
        }
        int[] bin = new int[numVariable]; //convert char Array to int Array
        for (int i : bin) bin[i] = 0;
        int temp = numVariable - 1;
        for (int i = chr.length - 1; i >= 0; i--) {
            bin[temp] = Character.digit(chr[i], 2);
            temp--;
        }
        return bin;
    }

    public Minterma createMinterma(int decimal) {
        Minterma minterma = new Minterma();
        minterma.addDecimal(decimal);
        int[] binaryVariable = convertToBinary(decimal);
        for (int j = 0; j < binaryVariable.length; j++) {
            Variable variable = new Variable();
            if (binaryVariable[j] == 0) {
                variable.setName(String.format("!X%s", j + 1));
            } else {
                variable.setName(String.format("X%s", j + 1));
            }
            variable.setValueBin(binaryVariable[j]);
            minterma.addVariable(variable);
        }
        return minterma;
    }

    private boolean searchNumberInFunction(int decimal) {
        boolean overlap = true;
        for (Integer decimalInFunction : function) {
            if (decimalInFunction == decimal) {
                overlap = false;
            }
        }
        return overlap;
    }

    public ArrayList<Minterma> createMenterms(int[] function) {
        ArrayList<Minterma> menterms = new ArrayList<>();
        for (Integer decimal : function) {
            menterms.add(createMinterma(decimal));
        }
        return menterms;
    }

    public ArrayList<Minterma> createMenterms() {
        ArrayList<Minterma> menterms = new ArrayList<>();
        for (Integer decimal : function) {
            menterms.add(createMinterma(decimal));
        }
        return menterms;
    }

    private int searchMinimumNumberVariables(ArrayList<Minterma> minterms) {
        int minVars = 0;
        for (Minterma minterma : minterms) {
            ArrayList<Variable> variables = minterma.getVariable();
            if (minVars == 0) {
                minVars = variables.size();
            }
            int numVars = variables.size();
            if (minVars > numVars) {
                minVars = numVars;
            }
        }
        return minVars;
    }

    public ArrayList<ArrayList[]> searchGluingNum(ArrayList<Minterma> minterms) { // return number gluing parts ArrayArrays
        ArrayList gluingNum = new ArrayList<ArrayList[]>();
        int minimumNumberVariables = searchMinimumNumberVariables(minterms);
        for (int i = 0; i < minterms.size() - 1; i++) {
            for (int j = i + 1; j < minterms.size(); j++) {
                Minterma mintermaFirst = minterms.get(i);
                Minterma mintermaSecond = minterms.get(j);
                int alignment = compareNameMinterms(mintermaFirst, mintermaSecond, minimumNumberVariables);
                if (alignment != 0 && minimumNumberVariables - 1 != 0) {
                    ArrayList[] temp = new ArrayList[2];
                    temp[0] = mintermaFirst.getDecimal();
                    temp[1] = mintermaSecond.getDecimal();
                    gluingNum.add(temp);
                }
            }
        }
        return gluingNum;
    }

    private int compareNameMinterms(Minterma first, Minterma second, int minimumNumberVariables) {
        ArrayList<Variable> variablesMintermaFirst = first.getVariable();
        ArrayList<Variable> variablesMintermaSecond = second.getVariable();
        int alignment = 0;
        if (variablesMintermaFirst.size() == minimumNumberVariables && variablesMintermaSecond.size() == minimumNumberVariables) {
            for (int i = 0; i < variablesMintermaFirst.size(); i++) {
                String nameVariableFirst = variablesMintermaFirst.get(i).getName();
                String nameVariableSecond = variablesMintermaSecond.get(i).getName();
                if (nameVariableFirst.equals(nameVariableSecond)) alignment++;
            }
        }
        if (alignment == minimumNumberVariables - 1 && minimumNumberVariables - 1 != 0) {
            for (int i = 0; i < variablesMintermaFirst.size(); i++) {
                String nameVariableFirst = variablesMintermaFirst.get(i).getName();
                String nameVariableSecond = variablesMintermaSecond.get(i).getName();
                if (compareStrings(nameVariableFirst, nameVariableSecond)) {
                    alignment = 0;
                }
            }
        } else alignment = 0;
        return alignment;
    }

    public boolean compareStrings(String str1, String str2) {
        boolean suit = true;
        if (str1.charAt(str1.length() - 1) == str2.charAt(str2.length() - 1)) {
            suit = false;
        }
        return suit;
    }

    public ArrayList<Minterma> gluing(ArrayList<Minterma> minterms, ArrayList<ArrayList[]> gluingNum, ArrayList<Minterma> newMinterms) {
        try {
            for (ArrayList[] gluingNumb : gluingNum) {
                if (gluingNumb.length != 2) throw new MinimizationException("No correct number minterms");
                Minterma mintermaFirst = searchMintema(minterms, gluingNumb[0]);
                Minterma mintermaSecond = searchMintema(minterms, gluingNumb[1]);
                Minterma newMinterma = gluingThoMinterms(mintermaFirst, mintermaSecond);
                newMinterms.add(newMinterma);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return newMinterms;
        }
        return newMinterms;
    }

    private Minterma gluingThoMinterms(Minterma mintermaFirst, Minterma mintermaSecond) {
        Minterma newMinterma = new Minterma();
        ArrayList<Integer> newDecimal = new ArrayList<>();
        newDecimal.addAll(mintermaFirst.getDecimal());
        newDecimal.addAll(mintermaSecond.getDecimal());
        newMinterma.setDecimal(newDecimal);

        ArrayList<Variable> variableFirstMinterma = mintermaFirst.getVariable();
        ArrayList<Variable> variableSecondMinterma = mintermaSecond.getVariable();
        for (Variable variableFirst : variableFirstMinterma) {
            for (Variable variableSecond : variableSecondMinterma) {
                if (variableFirst.getName().equals(variableSecond.getName())) {
                    newMinterma.addVariable(variableFirst);
                    continue;
                }
            }
        }
        return newMinterma;
    }

    private Minterma searchMintema(ArrayList<Minterma> minterms, ArrayList<Integer> number) {
        try {
            for (Minterma minterma : minterms) {
                if (minterma.getDecimal().equals(number)) return minterma;
            }
            throw new MinimizationException("Minterma not found");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Minterma> addNoGluing(ArrayList<Minterma> minterms, ArrayList<ArrayList[]> gluingNum, ArrayList<Minterma> newMinterms) {
        for (Minterma minterma : minterms) {
            boolean thereIs = false;
            ArrayList<Integer> valueDec = minterma.getDecimal();
            outerloop:
            for (ArrayList[] gluedNum : gluingNum) {
                for (ArrayList<Integer> valueNumGluing : gluedNum) {
                    if (valueDec.equals(valueNumGluing)) {
                        thereIs = true;
                        break outerloop;
                    }
                }
            }
            if (!thereIs) newMinterms.add(minterma);
        }
        return newMinterms;
    }

    public ArrayList<Minterma> deleteCopies(ArrayList<Minterma> minterms) {
        ArrayList<Integer> numToDelete = new ArrayList<>();
        if (minterms.size() >= 2) {
            for (int i = 0; i < minterms.size() - 1; i++) {
                for (int j = i + 1; j < minterms.size(); j++) {
                    String nameFirst = createName(minterms.get(i));
                    String nameSecond = createName(minterms.get(j));
                    if (nameFirst.equals(nameSecond)) {
                        if (!checkIntegerNumberInArrayList(j, numToDelete))
                        numToDelete.add(j);
                    }
                }
            }
            Collections.sort(numToDelete);
            if (numToDelete.size() != 0) {
                for (int i = numToDelete.size() - 1; i >= 0; i--) {
                    minterms.remove((int) numToDelete.get(i));
                }
            }
        }
        return minterms;
    }

    private boolean checkIntegerNumberInArrayList(Integer number, ArrayList<Integer> arrayList){
        for (Integer iter: arrayList) {
            if (iter == number) {
                return true;
            }
        }
        return false;
    }

    private String createName(Minterma minterma) {
        StringBuilder nameMnterma = new StringBuilder();
        ArrayList<Variable> variables = minterma.getVariable();
        for (Variable variable : variables) {
            nameMnterma.append(variable.getName());
        }
        return nameMnterma.toString();
    }

    public int[][] createTableOverlapping(ArrayList<Minterma> minterms, ArrayList<Minterma> defaultMinterms) {
        int[][] tableOverlapping = new int[minterms.size()][defaultMinterms.size()];
        for (int i = 0; i < minterms.size(); i++) {
            for (int j = 0; j < defaultMinterms.size(); j++) {
                Minterma mintermaFirst = minterms.get(i);
                Minterma mintermaSecond = defaultMinterms.get(j);
                int minimumVariables = mintermaFirst.getVariable().size();
                if (minimumVariables > mintermaSecond.getVariable().size()) {
                    minimumVariables = mintermaSecond.getVariable().size();
                }
                int alignment = compareNameMinterms(mintermaFirst, mintermaSecond);
                if (alignment == minimumVariables) {
                    tableOverlapping[i][j] = 1;
                } else tableOverlapping[i][j] = 0;
            }
        }
        return tableOverlapping;
    }

    private int compareNameMinterms(Minterma first, Minterma second) {
        ArrayList<Variable> variablesMintermaFirst = first.getVariable();
        ArrayList<Variable> variablesMintermaSecond = second.getVariable();
        int alignment = 0;
        for (Variable variableFirst : variablesMintermaFirst) {
            for (Variable variableSecond : variablesMintermaSecond) {
                String nameVariableFirst = variableFirst.getName();
                String nameVariableSecond = variableSecond.getName();
                if (nameVariableFirst.equals(nameVariableSecond)) alignment++;
            }
        }
        return alignment;
    }

    public ArrayList<Minterma> minimizationMetodPetric(int[][] tableOverlapping, ArrayList<Minterma> minMinterms, ArrayList<Minterma> defaultMinterms) {
        ArrayList[] sumMinterms = createSumLiterals(tableOverlapping, minMinterms, defaultMinterms);//(a+b)*(b+c)*(c+a)
        ArrayList<ArrayList> multiMinterms = multiplicationSumMinterms(sumMinterms);//(a*b*c)+(a*c*c)+...
        for (ArrayList<Minterma> minterms : multiMinterms) {
            deleteCopies(minterms);
        }
        return searchMinSize(multiMinterms);
    }

    private ArrayList[] createSumLiterals(int[][] tableOverlapping, ArrayList<Minterma> minMinterms, ArrayList<Minterma> defaultMinterms) {
        ArrayList[] sumMinterms = new ArrayList[defaultMinterms.size()];
        for (int i = 0; i < sumMinterms.length; i++) {
            sumMinterms[i] = new ArrayList<Minterma>();
        }
        for (int j = 0; j < defaultMinterms.size(); j++) {
            for (int i = 0; i < minMinterms.size(); i++) {
                if (tableOverlapping[i][j] == 1) {
                    sumMinterms[j].add(minMinterms.get(i));
                }
            }
        }
        return sumMinterms;
    }

    private ArrayList<ArrayList> multiplicationSumMinterms(ArrayList[] sumMinterms) {
        ArrayList<ArrayList> multiMinterms = new ArrayList<>();//(a*b*c)+(a*c*c)+...
        for (int i = 0; i < sumMinterms.length; i++) {
            ArrayList<Minterma> sum = sumMinterms[i];
            if (multiMinterms.size() == 0) {
                for (Minterma minterma : sum) {
                    ArrayList<Minterma> list = new ArrayList<>();
                    list.add(minterma);
                    multiMinterms.add(list);
                }
                continue;
            }
            ArrayList<ArrayList> newMultiMinterms = new ArrayList<>();
            for (Minterma inc : sum) {
                ArrayList<ArrayList> tempList = new ArrayList<>();
                tempList = addArrayListToArrayList(multiMinterms, tempList);
                for (ArrayList multiplier : tempList) {
                    multiplier.add(inc);
                }
                addArrayListToArrayList(tempList, newMultiMinterms);
            }
            multiMinterms = newMultiMinterms;
        }
        return multiMinterms;
    }

    private ArrayList addArrayListToArrayList(ArrayList<ArrayList> transmitter, ArrayList<ArrayList> receiver) {
        for (ArrayList<Minterma> list : transmitter) {
            ArrayList temp = new ArrayList();
            for (Minterma minterma : list) {
                temp.add(minterma);
            }
            receiver.add(temp);
        }
        return receiver;
    }

    private ArrayList<Minterma> searchMinSize(ArrayList<ArrayList> multiMinterms) {
        try {
            int min = multiMinterms.get(0).size();
            for (ArrayList arrayList : multiMinterms) {
                if (arrayList.size() < min) min = arrayList.size();
            }
            for (int i = 0; i < multiMinterms.size(); i++) {
                ArrayList arrayList = multiMinterms.get(i);
                if (arrayList.size() == min) {
                    return multiMinterms.get(i);
                }
            }
            new MinimizationException("Minimum size not fiund. Return zero minterma.");
        }catch (Exception e){
            e.printStackTrace();
            return multiMinterms.get(0);
        }
        return multiMinterms.get(0);
    }

    public int[] getFunction() {
        return function;
    }
}
