public class Variable {

    private String name;
    private int valueBin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValueBin() {
        return valueBin;
    }

    public void setValueBin(int valueBin) {
        this.valueBin = valueBin;
    }
}
