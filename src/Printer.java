import java.util.ArrayList;

public class Printer {

    public void printIntArray(int[] array) { //print int Array
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void printMinterma(Minterma minterma) {
        for (Variable variable : minterma.getVariable()) {
            System.out.print(variable.getName());
        }
        System.out.println();
        for (Variable variable : minterma.getVariable()) {
            System.out.print(variable.getValueBin());
        }
        System.out.println();
    }

    public void printMinterms(ArrayList<Minterma> minterms) {
        for (Minterma minterma : minterms) {
            ArrayList<Variable> variables = minterma.getVariable();
            for (Variable variable : variables) {
                System.out.print(variable.getName() + " ");
            }
            System.out.println("\t" + minterma.getDecimal() + "\n");
        }
        System.out.println();
    }

    public void printArrayArraysWithFormat(ArrayList<ArrayList[]> list) { // print int ArrayList
        for (ArrayList[] arrayList : list) {
            System.out.print(arrayList[0]);
            System.out.print("-");
            System.out.println(arrayList[1]);
        }
        System.out.println();
    }

    public void printThoLevelArray(int[][] table, int height, int length) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length; j++) {
                System.out.print(table[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private String createName(Minterma minterma) {
        StringBuilder nameMnterma = new StringBuilder();
        ArrayList<Variable> variables = minterma.getVariable();
        for (Variable variable : variables) {
            nameMnterma.append(variable.getName());
        }
        return nameMnterma.toString();
    }

    public void printMintermsTableOverlapping(ArrayList<Minterma> minterms, ArrayList<Minterma> defaultMinterms) {
        System.out.println("default f1(for fully function) or f0(for partially function):");
        for (Minterma minterma : defaultMinterms) {
            System.out.print(createName(minterma) + "\t");
        }
        System.out.println("\nf1min:");
        int height = minterms.size();
        for (int i = 0; i < height; i++) {
            System.out.println(createName(minterms.get(i)) + "\t");
        }
    }

    public void printMintepmsInString(ArrayList<Minterma> minterms) {
        for (Minterma minterma : minterms) {
            System.out.print(createName(minterma) + "\t\t");
        }
        System.out.println();
    }

    public void printArrayListArrayLists(ArrayList<ArrayList> listArrayList){
        for (ArrayList<Minterma> list: listArrayList) {
            printMintepmsInString(list);
            System.out.println();
        }
    }

    public void printArrayArrayLists(ArrayList[] arrayLists){
        for (int i = 0; i < arrayLists.length; i++) {
            printMintepmsInString(arrayLists[i]);
            System.out.println("\n");
        }
    }

    public void printIntegerArrayList(ArrayList<Integer> arrayList){
        for (Integer num: arrayList) {
            System.out.print(num + "\t");
        }
        System.out.println();
    }
}
