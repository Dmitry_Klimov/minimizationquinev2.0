public class MinimizationException extends Exception{
    public MinimizationException(String message) {
        super(message);
    }
}
