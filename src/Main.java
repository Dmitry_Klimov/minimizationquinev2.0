import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        //Минимизация методом Квайна
        int numVariable = 4;
        int[] f1 = {0,1,2,3,4,8,9,10,11,12,13,14,15}; //all 1,* and ~
        int[] f0 = {4}; //only 1
        Minimization minimization = new Minimization(f1, numVariable);
        //minimizationFullyFunction(minimization);
        minimizationPartiallyFunction(minimization, f0);

        /*
        Для минимизации частично заданной ФАЛ методом Квайна неоходимо:
        1. Из частично заданной функции f получить новую полностью заданную функцию f1
        2. Упростить функцию f1 обычным путем(сделать минимизацию методом Квайна)
        3. Составить таблицу перекрытий между элементарными конъюнкциями функции f0
           и упрощенной функции f1 и по этой таблице выбрать окончательную минимальную форму частично заданной функции f .
        */
    }

    private static ArrayList<Minterma> minimizationFullyFunction(Minimization minimization) {
        Printer printer = new Printer();
        ArrayList<Minterma> minterms = minimization.createMenterms();
        ArrayList<ArrayList[]> gluingNum;
        do {
            printer.printMinterms(minterms);
            gluingNum = minimization.searchGluingNum(minterms);
            printer.printArrayArraysWithFormat(gluingNum);
            ArrayList<Minterma> newMinterms = new ArrayList<>();
            newMinterms = minimization.addNoGluing(minterms, gluingNum, newMinterms);
            newMinterms = minimization.gluing(minterms, gluingNum, newMinterms);
            minterms = newMinterms;
        } while (gluingNum.size() != 0);
        minterms = minimization.deleteCopies(minterms);

        ArrayList<Minterma> defaultMinterms = minimization.createMenterms(minimization.getFunction());
        System.out.println("Table overlapping:");
        printer.printMintermsTableOverlapping(minterms, defaultMinterms);
        int[][] tableOverlapping = minimization.createTableOverlapping(minterms, defaultMinterms);
        printer.printThoLevelArray(tableOverlapping, minterms.size(), defaultMinterms.size());
        ArrayList<Minterma> ansverOfMinimization = minimization.minimizationMetodPetric(tableOverlapping,minterms,defaultMinterms);
        System.out.print("Ansver of minimization:\t");
        printer.printMintepmsInString(ansverOfMinimization);
        System.out.println();
        return ansverOfMinimization;
    }

    private static void minimizationPartiallyFunction(Minimization minimization, int[] f0){
        Printer printer = new Printer();
        ArrayList<Minterma> f1min = minimizationFullyFunction(minimization);

        ArrayList<Minterma> f0_ones = minimization.createMenterms(f0);
        System.out.println("Table overlapping:");
        printer.printMintermsTableOverlapping(f1min, f0_ones);
        int[][] tableOverlapping = minimization.createTableOverlapping(f1min, f0_ones);
        printer.printThoLevelArray(tableOverlapping, f1min.size(), f0_ones.size());
        ArrayList<Minterma> ansverOfMinimization = minimization.minimizationMetodPetric(tableOverlapping, f1min, f0_ones);
        System.out.print("Ansver of minimization:\t");
        printer.printMintepmsInString(ansverOfMinimization);
    }
}
